var mongoose = require('mongoose');

var BookSchema = new mongoose.Schema({
  title: String,
  price: {type: Number, default: 0},
  upvotes: {type: Number, default: 0},
  downvotes: {type: Number, default: 0},
  comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}]
});

BookSchema.methods.upvote = function(cb) {
  this.upvotes += 1;
  this.save(cb);
};

mongoose.model('Book', BookSchema);
