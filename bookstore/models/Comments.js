var mongoose = require('mongoose');

var CommentSchema = new mongoose.Schema({
  body: String,
  author: String,
  upvotes: {type: Number, default : 0},
  rating: {type: Number, default: 0},
  book: {type: mongoose.Schema.Types.ObjectId, ref: 'Book'}
});

CommentSchema.methods.upvote = function(cb){
  this.upvotes += 1;
  this.save(cb);
};

mongoose.model('Comment', CommentSchema);
