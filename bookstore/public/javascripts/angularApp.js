var app = angular.module('onlineBookstore', ['ui.router']);

app.factory('books', ['$http', function($http) {
  var o = {
    books: []
  };
  return o;

  o.getAll = function(){
    return $http.get('/books').success(function(data){
      angular.copy(data, o.books);
    });
  };
}]);

app.controller('MainCtrl', [
  '$scope',
  'books',
  function($scope, books) {
    $scope.books = books.books
    $scope.addBook = function() {
      if (!$scope.bookTitle || $scope.bookTitle === '') {
        alert("Please enter a title");
        return;
      }
      if (!$scope.bookPrice || $scope.bookPrice === '') {
        alert("Please enter a price");
        return;
      }
      $scope.books.push({
        title: $scope.bookTitle,
        price: $scope.bookPrice,
        upvotes: 0,
        downvotes: 0,
        comments: [{
          author: 'Tano',
          body: 'Cool book',
          upvotes: 0,
          rating: 10
        }, {
          author: 'Bob',
          body: 'Kon veel beter',
          upvotes: 0,
          rating: 3
        }]
      });
      $scope.bookTitle = "";
      $scope.bookPrice = "";
    }
    $scope.upvote = function(book) {
      book.upvotes += 1;
    };
  }
]);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: '/home.html',
      controller: 'MainCtrl',
      resolve: {
        bookPromise: ['books', function(books){
          return o.getAll();
        }]
      }
    }).state('books', {
      url: '/books/{id}',
      templateUrl: '/books.html',
      controller: 'BooksCtrl'
    });
  $urlRouterProvider.otherwise('home');
}]);

app.controller('BooksCtrl', [
  '$scope',
  '$stateParams',
  'books',
  function($scope, $stateParams, books) {
    console.log(books.books[$stateParams.id]);
    $scope.book = books.books[$stateParams.id];
    $scope.addComment = function() {
      if(!$scope.body || $scope.body === ''){
        alert("Add a comment!");
        return;
      };
      if(!$scope.rating || $scope.rating === ''){
        alert("Add a rating!");
        return;
      };
      $scope.book.comments.push({
        body: $scope.body,
        upvotes: 0,
        author: 'user',
        rating: $scope.rating
      });
    };
    $scope.body = '';
    $scope.rating = '';
  }
]);
