var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Book = mongoose.model('Book');
var Comment = mongoose.model('Comment');

//Router params for books
router.param('book', function(req, res, next, id) {
  var query = Book.findById(id);

  query.exec(function(err, book) {
    if (err) {
      return next(err);
    }
    if (!book) {
      return next(new Error("Can\'t find a book with that id..."));
    }
    req.book = book
    return next();
  });
});

//Router param for comments
router.param('comment', function(req, res, next, id) {
  var query = Comment.findById(id);

  query.exec(function(err, comment) {
    if (err) {
      return next(err);
    }
    if (!comment) {
      return next(new Error("Can\'t find a comment with that id..."));
    }
    req.comment = comment;
    return next();
  });
});

//Get all books
router.get('/books', function(req, res, next) {
  Book.find(function(err, books) {
    if (err) {
      return next(err);
    }
    res.json(books);
  });
});

//Get all Comments
router.post('/books/:book/comments', function(req, res, next) {
  var comment = new Comment(req.body);
  comment.book = req.book;

  comment.save(function(err, comment) {
    if (err) {
      return next(err);
    }
    req.book.comments.push(comment);
    req.book.save(function(err, book) {
      if (err) {
        return next(err);
      }
      res.json(comment);
    });
  });
});

//Post a new book
router.post('/books', function(req, res, next) {
  var book = new Book(req.body);

  book.save(function(err, book) {
    if (err) {
      return next(err);
    }
    res.json(book);
  });
});

//Get book by Id
router.get('/books/:book', function(req, res, next) {
  req.book.populate('comments', function(err, book) {
    if (err) {
      return next(err);
    }
    res.json(book);
  });
});

//upvote book with id
router.put('/books/:book/upvote', function(req, res, next) {
  req.book.upvote(function(err, book) {
    if (err) {
      return next(err);
    }
    res.json(book);
  });
});

//upvote comment from book with id and comment with id
router.put('/books/:book/comments/:comment/upvote', function(req, res, next) {
  req.comment.upvote(function(err, comment) {
    if (err) {
      return next(err);
    }
    res.json(comment);
  });
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: 'Express'
  });
});

module.exports = router;
